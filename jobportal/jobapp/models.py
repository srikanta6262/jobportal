from django.db import models

# Create your models here.


class Jobs(models.Model):
    job_titles = models.CharField(max_length=100, blank=True)
    company_name = models.CharField(max_length=100, blank=True)
    work_experience = models.CharField(max_length=100, blank=True)
    location = models.CharField(max_length=100, blank=True)
    key_skill = models.CharField(max_length=100, blank=True)
    qualification = models.CharField(max_length=100, blank=True)
    salary = models.FloatField(blank=True)
    number_of_openings = models.IntegerField(blank=True)
    date_of_post = models.DateTimeField(auto_now_add=True)
    end_date = models.DateTimeField()
    description = models.TextField(blank=True)
