from django.shortcuts import render
from django.shortcuts import render

from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser
from rest_framework import status

from .models import Employee
from .serializers import empSerializer
from rest_framework.decorators import api_view

# Create your views here.
@api_view(['GET', 'POST'])
def emp_list(request):
    if request.method == 'GET':
        data1 = Employee.objects.all()

        name = request.GET.get('name', None)
        if name is not None:
            data1 = data1.filter(name=name)

        tutorials_serializer = empSerializer(data1, many=True)
        return JsonResponse(tutorials_serializer.data, safe=False)
        # 'safe=False' for objects serialization
    elif request.method == 'POST':
        emp_data = JSONParser().parse(request)
        tutorial_serializer = empSerializer(data=emp_data)
        if tutorial_serializer.is_valid():
            tutorial_serializer.save()
            return JsonResponse(tutorial_serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(tutorial_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def emp_detail(request,pk):
    # find tutorial by pk (id)
    try:
        empdata = Employee.objects.get(name=pk)
        print(empdata.mobile)
    except Employee.DoesNotExist:
        return JsonResponse({'message': 'The tutorial does not exist'}, status=status.HTTP_404_NOT_FOUND)
    if request.method == 'PUT':
        emp = JSONParser().parse(request)
        emp_serializer = empSerializer(empdata, data=emp)
        if emp_serializer.is_valid():
            emp_serializer.save()
            return JsonResponse(emp_serializer.data)
        return JsonResponse(emp_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
