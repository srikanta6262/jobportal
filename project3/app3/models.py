from django.db import models


class EmpModel(models.Model):
    name = models.CharField(max_length=250, blank=True)
    designation = models.CharField(max_length=200, blank=True)
    address = models.CharField(max_length=200, blank=True)
    mobile = models.IntegerField(primary_key=True)
