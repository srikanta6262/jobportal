from rest_framework import serializers
from .models import Employee

class EmployeeSerializer(serializers.ModelSerializer):
    # specify model and fields
    class Meta:
        model = Employee
        fields = '__all__'