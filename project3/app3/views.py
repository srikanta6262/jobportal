from django.shortcuts import render

# Create your views here.
from rest_framework.response import Response
from rest_framework import viewsets


class TestViewSet(viewsets.ViewSet):

    def list(self, request):
        colors = ['RED', 'GREEN', 'YELLOW', 'ORANGE']
        return Response({'msg': 'Wish YOu Colorful Life in 2019', 'colors': colors})
