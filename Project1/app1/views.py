from django.shortcuts import render
from .models import Employee
from django.http import HttpResponse
from django.views import View

import json
from django.http import JsonResponse



# Create your views here.
class First(View):
    def post(self,request):
        payload = json.loads(request.body)
        name=payload['name']
        des=payload['designation']
        add=payload['address']
        mob=payload['mobile']
        try:
            emp=Employee.objects.create(name=name,designation=des,address=add,mobile=mob)
            return JsonResponse({'status':400})
        except:
            return JsonResponse({"message:":"dataExist"})


    def get(self,request):
        payload = json.loads(request.body)
        try:
            data=Employee.objects.filter(name=payload['name'])[0]
            print(data.mobile)
            name={'Mobile': data.mobile,"name":data.name,"addres":data.address}
            return JsonResponse(name)
        except :
            return JsonResponse({'message':'No Data Found'})


    def delete(self,request):
        payload = json.loads(request.body)
        try:
            data=Employee.objects.get(name=payload['name'])
            data.delete()
            return JsonResponse({'status':'deleted'})
        except:
            return JsonResponse({"message:":"No dataFound"})


    def put(self,request):
        payload = json.loads(request.body)
        try:
            data=Employee.objects.get(mobile=payload['mobile'])
            data.name=payload['name']
            data.designation=payload['designation']
            data.address=payload['address']
            data.save()
            return JsonResponse({'status':'Updated'})
        except:
            return JsonResponse({"message:":"No dataFound"})



